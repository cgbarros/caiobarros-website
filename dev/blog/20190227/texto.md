<head>
<link rel="stylesheet" href="../../style.css">
<title>Remover arquivos no Google Drive automaticamente</title>
</head>

# Remover atuomaticamente arquivos e pastas antigos no Google Drive

<div class="date-text">27/02/2019</div>

<div class="back-links">
[.voltar para o blog](/blog.html)

[..voltar para a página inicial](/index.html)
</div>

Há pouco tempo gastei algumas horas finalmente organizando meu Google Drive (apesar do Google incentivar usar a ferramenta de busca do próprio Drive, ainda prefiro navegar por pastas que fazem sentido e ter meus arquivos organizados). Uma das coisas mais difíceis de organizar, sem dúvida, foi a pasta de documentos, principalmente porque foi necessário bolar categorias de pastas relevantes para os arquivos, de uma maneira que ficasse óbvio onde cada documento está.

Olhando meus documentos, percebi que seria interessante separar uma pasta para comprovantes como notas fiscais, garantias, contas, etc. Esses documentos possuem uma data de validade: no Brasil, normalmente após cinco anos já não possuem valor. As regras para isso mudam de documento pra documento mas essa é a regra geral e para mim seria suficiente *remover arquivos com mais de 5 anos* dentro dessa pasta (supondo, é claro, que o Google Drive ainda existirá em 5 anos). Eu poderia fazer isso manualmente: todo ano eu acessaria a pasta e removeria os documentos antigos. Mas, apesar de não ser uma quantidade grande de trabalho, não é dos mais agradáveis, além de ser fácil de esquecer ou deixar para depois indefinidamente. Além disso, achei que seria uma oportunidade de aprender a utilizar os [Google Apps Scripts](https://www.google.com/script/start/).

Os [Google Apps Scripts](https://www.google.com/script/start/) são uma ferramenta que permite interagir com outros produtos do Google utilizando código JavaScript. Para começar a usar é só acessar os [Google Apps Scripts](https://www.google.com/script/start/) e clicar em "Start Scripting". 

![Botão Start Scripting](start.png)

Se você já estiver logado em uma conta do Google, será levado diretamente para a tela de edição de scripts.

![Tela de edição de scripts](edit_scripts.png)

Vários produtos do Google permitem a utilização de scripts para expandir sua funcionalidade. Isso permite automatizar tarefas complexas e evitar erros humanos. Para aprender mais detalhadamente como utilizar os scripts, há uma [página de referência](https://developers.google.com/apps-script/overview) com tutoriais e exemplos para cada aplicação. 

## Configurações antes de acionar o script

Com a tela de edição de scripts, já podemos começar a programar. No entanto, para esse script em particular, são necessárias algumas configurações para que ele funcione.

Antes de tudo, como utilizaremos o Google Drive, precisamos habilitar o serviço da Drive API. Para fazer isso, é necessário navegar para Resources > Advanced Google services...

![Menu Advanced Google Services](advanced_services.png)

e habilitar a Drive API mudando o botão para a posição *on* e clicar em *ok*.

![Drive API habilitada](drive_api.png)

Além disso, vamos precisar do ID da pasta do Google Drive onde os documentos estão armazenados. Para saber o ID de uma pasta do Google Drive, navegue até ela. O ID é o código que aparece na barra de endereço do navegador, após o separador */folders/*. na imagem abaixo eu escondi o ID da pasta que estou utilizando para evitar ataques, mas o local onde encontrá-lo está lá:

![Id da pasta do Google Drive](folderId.png)

E agora é só copiar o script abaixo. Esse ambiente de edições é na verdade uma [IDE](https://pt.wikipedia.org/wiki/Ambiente_de_desenvolvimento_integrado) com recurso de realce de sintaxe, recuo automático, auto completar, etc. que permitem explorar as possibilidades de código. Uma coisa que eu reparei é que o auto completar sabe quais são as APIs habilitadas, o que facilita saber se é necessário ativar mais algum serviço ou não.

## O script

Aqui o script completo. Basta copiar e colar. Lembre-se de substituir o campo [idDaPasta] pelo id correspondente da pasta onde os documentos serão armazenados.

~~~~
//remove all documents older than 5 years
function cleanOldDocuments() {
  var folder = DriveApp.getFolderById('[idDaPasta]');
  var subfolders = folder.getFolders();
  var files = folder.getFiles();
  
  var d = new Date();
  var year = d.getFullYear();
  var month = d.getMonth();
  var day = d.getDate();
  var fiveYearsBack = new Date(year - 5, month, day)
  
  while (files.hasNext()) {
    var file = files.next();
    if (file.getLastUpdated() < fiveYearsBack) {
      Drive.Files.trash(file.getId());
    }
  }
  
  while (subfolders.hasNext()) {
    var subfolder = subfolders.next();
    if (subfolder.getLastUpdated() < fiveYearsBack) {
      Drive.Files.trash(subfolder.getId());
    }
  }
}
~~~~

## Entendendo cada parte

### Selecionando arquivos e pastas

Com a API do Drive habilitada, ao utilizarmos o `DriveApp` podemos selecionar pastas ou arquivos para trabalhar neles. Aqui o método `getFolderByID` é bem autoexplicativo: ele seleciona uma pasta do Google Drive passando o ID dela como argumento. Nomeei esse método com a variável `folder` e, a partir dela, obtive seus arquivos -- `folder.getFiles()` -- e sub-pastas (pastas dentro da pastas) --  `folder.getFolders()`. 

As primeiras três variáveis são basicamente isso: uma variável contendo um método que retorna uma pasta do Google drive, e as outras duas que contém os arquivos e sub-pastas dentro dessa pasta.

~~~~
  var folder = DriveApp.getFolderById('[idDaPasta]');
  var subfolders = folder.getFolders();
  var files = folder.getFiles();
~~~~

### Definindo datas

Como queremos apagar os arquivos com mais de 5 anos, é necessário definir no código a data de 5 anos atrás. O JavaScript já possui alguns métodos para fazer isso. Primeiro definimos uma nova variável que retorna a data atual -- `var d = new Date();` - e a partir daí podemos extrair o dia, mês e ano e criar uma nova data igual à atual, porém 5 anos atrás.

~~~~
  var d = new Date();
  var year = d.getFullYear();
  var month = d.getMonth();
  var day = d.getDate();
  var fiveYearsBack = new Date(year - 5, month, day)
~~~~

Note que, devido à maneira como JavaScript implementa datas, fazer apenas `new Date(year - 5)` não funciona. Possivelmente há outras maneiras de implementar a data de 5 anos atrás, porém não tenho certeza de como fazer isso :D

### Apagar os arquivos antigos com um while loop

Agora que já temos uma lista com todos os arquivos e subpastas que queremos verificar e a data, é só construir um condicional que remova os arquivos cuja data seja anterior a 5 anos. 

~~~~
  while (files.hasNext()) {
    var file = files.next();
    if (file.getLastUpdated() < fiveYearsBack) {
      Drive.Files.trash(file.getId());
    }
  }
~~~~

O bloco `while` aqui significa, basicamente que enquanto (`while`) a lista de arquivos (`files`) tiver mais algum elemento (`hasNext()`) o código dentro do bloco será aplicado (e, depois de aplicar o código, o bloco vai passar para o próximo elemento da lista `files`).

Dentro do bloco, então, colocamos um condicional (`if`) que testa se a última atualização do arquivo foi feita há mais de 5 anos atrás: `file.getLastUpdated() < fiveYearsBack`. Se isso for verdade, então removemos o arquivo: `Drive.Files.trash(file.getId())`. Essa última linha aplica o método `trash` (mover para a lixeira) ao arquivo cujo ID é recuperado usando o método `file.getId()`.

O último bloco é exatamente igual, porém excluindo as subpastas com mais de 5 anos ao invés de arquivos:

~~~~
  while (subfolders.hasNext()) {
    var subfolder = subfolders.next();
    if (subfolder.getLastUpdated() < fiveYearsBack) {
      Drive.Files.trash(subfolder.getId());
    }
  }
~~~~

Talvez seja possível economizar linhas de código criando uma função um pouco mais abstrata. Em vez de repetir o código, escreveríamos

~~~~
function removeOlderThanFiveYear(itens) {
  while (itens.hasNext()) {
    var item = itens.next();
    if (item.getLastUpdated() < fiveYearsBack) {
      Drive.Files.trash(item.getId());
    }
  }
}

removeOlderThanFiveYears(files);
removeOlderThanFiveYears(subfolders);
~~~~

O código acima deve funcionar (apesar de eu não ter testado ainda) mas, de qualquer maneira, para um script tão simples como esse, a repetição do código não tem tanto problema.

## Acionar o script uma vez ao dia

Agora que o script está montado e salvo, ele precisa ser executado várias vezes ao ano para que os arquivos antigos sejam apagados. O sistema do Google permite criar acionadores que executarão os scripts com certa frequência. No nosso caso, vamos configurar uma execução diária à meia-noite. Dessa maneira todo dia o script verificará nossa pasta de documentos e enviará para a lixeira os arquivos com mais de 5 anos. É claro que esse período pode ser diferente, já que 5 anos é um tempo razoavelmente longo.

Para a configurar os acionadores do script atual, vamos navegar para Edit > Current project's triggers.

![Menu de acionadores](doc_triggers.png)

Em seguida, configuramos o acionador para que execute o script uma vez ao dia, à meia noite.

![Acionar o script uma vez ao dia](triggers.png)

<div class="back-links">
[.voltar para o blog](/blog.html)

[..voltar para a página inicial](/index.html)
</div>